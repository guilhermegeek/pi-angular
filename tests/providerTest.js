describe('Test pi provider', function () {

    var piAppProvider;

    beforeEach(function () {

        angular
            .module('test', function(){})
            .config(function(_piAppProvider_) {
                piAppProvider = _piAppProvider_;
            });

        module('pi', 'test');

        inject(function(){});

    });

        it('tests the providers internal function', function () {
            // check sanity
            expect(piAppProvider).not.toBeUndefined();
            // configure the provider
            //piAppProvider.mode('local');
            // test an instance of the provider for 
            // the custom configuration changes
            expect(piAppProvider.hasModule('non-existing')).toBe(false);

            piAppProvider.addModule('non-existing');
            expect(piAppProvider.hasModule('non-existing')).toBe(true);
        });

});