/**
 * @ng-doc service
 * @name SEO Validator
 *
 * @description
 * Helper class to validate SEO
 * Functions started with "validate" will be used agains validateObject() with common properties array
 */
 var seoValidationResult = {
 	message: '',
 	error: false
 };
 
(function(){
	var seoValidator = function(commonUtils){
		var self = this,
			commonProperties = ['title', 'content', 'url', 'excerpt'];

		this.validateTitle = function(title) {
			var errors = [];
			
			if(title.length < 30 || title.length > 70) {
				errors.push('Title length should be between 30 and 70');
			}
		};

		this.validateContent = function(content) {

		};

		this.validateUrl = function(url){

		};

		/*
		 * Runs all the validation functions of the service agains the object
		 * The existence of the properties is checked, and the object must have the regular properties: title, content, url, etc
		 */
		this.validateObject = function(obj) {
			var results = [];
			angular.forEach(commonProperties, function(prop, key) {
				if(prop in obj) {
					var fn = "validate" + commonUtils.capitalizeFirstLetter(prop);
					results.push(fn(prop));
				}
			});
		};
	};

	seoValidator.$inject = ['commonUtils'];

	angular
		.module('pi')
		.factory('seoValidator', seoValidator);
})();