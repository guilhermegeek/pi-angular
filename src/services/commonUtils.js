(function(){
	var commonUtils = function(){

		this.capitalizeFirstLetter = function(string) {
			return string.charAt(0).toUpperCase() + string.slice(1);
		};
	};


	angular
		.module('pi')
		.factory('commonUtils', commonUtils);
})();